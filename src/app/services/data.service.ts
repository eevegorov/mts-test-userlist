import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { DEFAULT_AVATAR } from '../config';
import { UserDataExtended, UserDataResponse, UserTodo } from '../models';
import { ApiService } from './api.service';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private apiService: ApiService) { }

  public getUsers(): Observable<UserDataExtended[]> {
    return this.apiService.getUsers().pipe(
      map((users: UserDataResponse[]) => users.map(user => ({
        ...user,
        avatar: DEFAULT_AVATAR,
        address: `${user.address.city}, ${user.address.street}, ${user.address.suite}`
      }))),
      catchError(this.handleError)
    );
  }

  public getUser(id: number): Observable<UserDataExtended> {
    return this.apiService.getUser(id).pipe(
      map((users: UserDataResponse[]) => users.length ? users[0] : null),
      map((user: UserDataResponse) => (user ? {
        ...user,
        avatar: DEFAULT_AVATAR,
        address: `${user.address.city}, ${user.address.street}, ${user.address.suite}`
      } : null)),
      catchError(this.handleError)
    );
  }

  public getUserTodos(userId: number): Observable<UserTodo[]> {
    return this.apiService.getUserTodos(userId).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse): Observable<any> {
    console.log('Http request error: ', error);
    return throwError('HTTP Error');
  }
}
